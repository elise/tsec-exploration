#!/bin/sh

rm -rf ./launcher
cp -r ../launcher ./
cp postrun.h ./launcher/src/

envyas -m falcon -V fuc5 -F crypt -i -o out ./boot.asm \
    && envydis -m falcon -V fuc5 -F crypt -b 0xF00 -i out \
    && envyas -m falcon -V fuc5 -F crypt -i -o out_kg_sig_rop kg_sig_rop.asm \
    && nx-tsec-append-blob -b 4608 "$1" out ./launcher/src/tsec-fw.bin \
    && rm out \
    && nx-tsec-append-blob ./launcher/src/tsec-fw.bin out_kg_sig_rop ./launcher/src/tsec-fw.bin \
    && rm out_kg_sig_rop \
    && make -j8 -C ./launcher clean \
    && make -j8 -C ./launcher \
    && mv ./launcher/out/tsec_payload.bin . \

rm -rf ./launcher 
