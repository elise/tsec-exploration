# Keygen ROP

This is an exploit for the NX keygen TSEC blob that allows you to exfiltrate a fakesigning key.

## Usage

### Prerequesites

* devkitARM
* [nx-tsec-append-blob](https://gitlab.com/elise/nx-tsec-append-blob)
* 3.0.2 TSEC Firmware

Run:

```sh
./build.sh <path_to_302_tsec>
```

Push the resulting `tsec_payload.bin` using a fusee launcher, follow the instructions on screen. The key will be displayed on screen and also written to `fakesigning-key.bin`.
