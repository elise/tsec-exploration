# Interesting Behaviour

## Trivial Secure ROP

You can control the return address from a secure blob by branching into it instead of calling it as branching does not push the return address to the stack which allows you to push your own ROP chain. As heavy secure mode is not dropped until the PC points outside of authenticated pages this allows for trivial ROP in heavy secure payloads.

## Crypto registers are decoded as 3 bytes in Xfers

The upper 29 bits of the register are ignored and can be set to anything.
