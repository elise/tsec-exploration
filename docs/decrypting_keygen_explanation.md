# Decrypting Keygen

## How is it regularly decrypted

Under normal circumstances the keygen blob is decryped using AES-128-CBC with an empty IV. The key is generated using the `gen_usr_key` routine which uses a routine like the following:

``` asm
// $c0 contains the seed of "CODE_ENC_01\x00\x00\x00\x00\x00"
csecret $c1 0x26 // Loads hardware secret 0x26 into $c1
ckeyreg $c1 // Sets the key register as $c1
cenc $c1 $c0 // Encrypts the seed with the active key register ($c1) and stores the result in $c1 
csigenc $c1 $c1 // Encrypts the current signature with the result and stores the result in $c1
```

## Vulnerability

KeygenLDR attempts to verify the boot stage. It does this by copying it to data memory and then calculating a signature over it.

We can set the size of the boot stage to be the size of our entire firmware and place the stack at a location where data from the end of our firmware that we control will be copied over the stack and we can overwrite the return address with our ROP chain.

The function has the signature `memcpy_i2d(dmem_addr, imem_addr, size)` and is called with `(0, 0, CONTROLLABLE)` in our case.

## Exploitation

Our goals are to:

1. Call `gen_usr_key` with args `(1, !=0)`
2. Call `crypto_load` with args `(1, 0x10-Aligned)`, this will write the content of the crypto register specified in $r10 to the address in $r11
3. Bring SCP (Secure Coprocessor) out of lockdown via an MMIO write so we can read TSEC memory
4. Return to our boot payload

When the `memcpy_i2d` function returns `$r11` will be non-zero as it increases `$r11` by 4 with every 4 bytes it reads. This leaves us with needing to set `$r10` to 1.

There is a really nice gadget in the `memcmp` routine that sets $r10 to 1. It looks like:

```asm
mov $r10 0x1
ret
```

We can then call `gen_usr_key` which will leave the key we want to exfiltrate in both crypto registers 1 and 4.

To read the key out we first want to set `$r10` to 0x1 using the `memcmp` gadget previously shown and then call `crypto_load` (`gen_usr_key` leaves `$r11` with a stack address that should be 0x10 aligned as long as your initial stack pointer was).

Memory access is currently still restricted though as the SCP is in lockdown though we will need to bring it out of lockdown. There is a gadget at the end of the KeygenLDR that does this before returning.

This is all we need to do before returning to our boot payload where the key is now on the stack.

This leaves our ROP chain looking like this on 3.0.2:

```asm
.b32 0x5b8 // Gadget that sets $r10 to 1
.b32 0x647 // gen_usr_key function, needs $r10 to be 1, needs $r11 to be != 0
.b32 0x5b8 // Gadget that sets $r10 to 1
.b32 0x5bd // crypto_load function, takes keyslot in first 3 bits of $r10, takes 0x10-Aligned output buffer in $r11
.b32 0x3be // Bring memory out of lockdown
.b32 0xf76 // Address for success in boot.asm
```

## Side Note

KeygenLDR clears the active signature before returning to the boot payload so we need to get ROP before it returns rather than using the ["Trival Secure ROP" described here](https://gitlab.com/elise/tsec-exploration/-/blob/main/docs/interesting_behaviour.md).
