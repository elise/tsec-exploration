# Decrypting Keygen

This is a simple guide to obtaining the key used to encrypt the keygen TSEC firmware blob and then decrypting the blob. This does not explain how things work, [there is an explanation here](./decrypting_keygen_explanation.md).

## Obtaining 3.0.2 TSEC Firmware

First you need a copy of a TSEC firmware from HOS version 3.0.2. The easiest way to obtain this is via extracting it from a firmware update for 3.0.2.

You can run this to extract a `tsec-fw.bin` in your current directory:

```sh
hactool <path_to_firmware_update>/594f7ccbeaed8a272b262789a0cf6e2b.nca --section0dir=system
nx-tsec-firmware-splitter -e <path_to_firmware_update>/system/nx/package1
```

## Building the payload

You will need to clone this repository and enter the `code_enc_01_rop` folder. You can then follow the build instructions in the README there using the TSEC firmware you extracted in the previous step.

## Executing the payload

You then need to push the resulting `tsec_payload.bin` with a fusee launcher and follow the instructions on screen. It will ask you to press any button (volume/power) to start the exploit and then the key should both be displayed on the screen and also written to `/keygen-enc-key.bin` on your SD.

As a sanity check: `sha256(key) = 6aa5865d1d3214f8e9b99f9a64754fcb4e868f4de49c3035e2ac9c1e0de35e59`

## Decrypting keygen

Now that you have the key you can extract the encrypted keygen payload by running:

```sh
nx-tsec-firmware-splitter <path_to_tsec_firmware>
nx-tsec-decrypt-keygen ./0x0A00_0x0F00_keygen_encrypted.bin ./0x0900_0x0E00_keygen.bin <previously_obtained_key>
```

This should leave the decrypted keygen blob at `./0x0900_0x0E00_keygen.bin`.
