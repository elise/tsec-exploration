#!/bin/sh

rm -rf ./launcher
cp -r ../launcher ./
cp postrun.h ./launcher/src/

envyas -m falcon -V fuc5 -F crypt -i -o out ./boot.asm \
    && nx-tsec-append-blob "$1" out ./launcher/src/tsec-fw.bin \
    && nx-tsec-append-blob ./launcher/src/tsec-fw.bin "$2" ./launcher/src/tsec-fw.bin \
    && nx-tsec-append-blob ./launcher/src/tsec-fw.bin "$3" ./launcher/src/tsec-fw.bin \
    && rm out \
    && make -j8 -C ./launcher clean \
    && make -j8 -C ./launcher \
    && mv ./launcher/out/tsec_payload.bin . \

rm -rf ./launcher 
