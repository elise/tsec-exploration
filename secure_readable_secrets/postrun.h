/*
* Copyright (c) 2023 EliseZeroTwo <mail@elise.moe>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms and conditions of the GNU General Public License,
* version 2, as published by the Free Software Foundation.
*
* This program is distributed in the hope it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _POSTRUN_H_
#define _POSTRUN_H_

#include "hwinit.h"
#include "fs.h"

const unsigned int TSEC_ENTRYPOINT = 0xF00;

void postrun(tsec_res_t* run) {
    print("\nSecrets: \n");
    unsigned char *base = (unsigned char*)(run->dmem + 0x2100);
    for (int s = 0; s < 0x40; s++) {
        print("%02x: ", s);
        for (int x = 0; x < 0x10; x++) {
            print("%02x", base[(s * 0x10) + x]);
        }
        print("\n");
    }

    if (!sdMount()) {
        print("No SD card, can't write secret-file\n");
    } else {
        if (!fopen("/csecrets.bin", "wb")) {
            print("Failed to open csecrets.bin for writing\n");
        } else {
            if (!fwrite(base, 0x10 * 0x40, 1)) {
                print("Failed to write csecrets.bin\n");
            }
            fclose();
        }
    }
}

#endif