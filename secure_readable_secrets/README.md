# Secure Readable Hardware Secrets

This exploits a blob in the Shield's HDCP firmware to obtain all hardware secrets that are readable in heavy secure mode

## Usage

### Prerequesites

* devkitARM
* [nx-tsec-append-blob](https://gitlab.com/elise/nx-tsec-append-blob)
* 3.0.2 TSEC Firmware
* Extracted blob from Shield HDCP firmware that's meant to be at 0x5f00 (SHA256: `4e55cab204625f43d6adac62cddaa3bb95ba3216176ae2a6f1d339b203c43687`)

Run:

```sh
./build.sh <path_to_302_tsec> <path_to_hdcp_5f00_blob> <path_to_hdcp_5f00_blob_signature>
```

Push the resulting `tsec_payload.bin` using a fusee launcher, follow the instructions on screen. The secrets will be displayed on screen, unreadable ones will be all-zero, and also written to `csecrets.bin` (0x10 bytes each, 0x40 entries).
