# CODE_ENC_01 ROP

A very simple ROP chain for exfiltrating the key used to encrypt the keygen TSEC firmware blob.

## Usage

### Prerequesites

* devkitARM
* [nx-tsec-append-blob](https://gitlab.com/elise/nx-tsec-append-blob)
* 3.0.2 TSEC Firmware

Run:

```sh
./build.sh <path_to_302_tsec>
```

Push the resulting `tsec_payload.bin` using a fusee launcher, follow the instructions on screen. The key will be displayed on screen and also written to `keygen-enc-key.bin`.

## Note

This can easily be adjusted for other firmwares by changing offsets.
