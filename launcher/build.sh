#!/bin/sh

envyas -m falcon -V fuc5 -F crypt -i -o out.a ./boot.asm \
    && nx-tsec-append-blob "$1" out.a ./src/tsec-fw.bin \
    && rm out.a \
    && make -j8 \
